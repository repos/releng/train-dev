#!/usr/bin/env python3

import flask

app = flask.Flask(__name__)


@app.route("/logstash-*/_search", methods=["POST"])
def search():
    j = flask.request.get_json()

    if j.get("size") == 0 and j.get("aggs"):
        with open("history.json") as f:
            return f.read()

    if j.get("size") == 500:
        with open("query-result.json") as f:
            return f.read()

    return "unexpected request", 400
