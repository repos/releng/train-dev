#!/bin/bash
set -eu -o pipefail

# 11212: local memcached
# 11213: stand-in for mcrouter
# both referenced by wmf-config/mc.php
/usr/bin/memcached -u memcache -l 127.0.0.1 -p 11212 -d
/usr/bin/memcached -u memcache -l 127.0.0.1 -p 11213 -d

exec /usr/sbin/php-fpm7.4 --nodaemonize --fpm-config /etc/php/7.4/fpm/php-fpm.conf
