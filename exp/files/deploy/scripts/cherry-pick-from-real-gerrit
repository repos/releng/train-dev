#!/bin/bash

set -eu

function usage {
    cat <<EOF
Usage: $0 changeNumber[,patchset]

$0 cherry-picks a particular (default: latest) patchset of
changeNumber from the real Gerrit server into the repo in the current
directory.

EOF
    exit 1
}

if [ $# -ne 1 ]; then
    usage
fi

OLDIFS="$IFS"

IFS=,
set "$1"
IFS="$OLDIFS"

change="$1"
patchset="${2:-}"

cp ~/.gitconfig ~/.gitconfig.save.$$
# shellcheck disable=SC2064
trap "mv ~/.gitconfig.save.$$ ~/.gitconfig" EXIT
git config --remove-section --global url.git://git/ 2>/dev/null || true
git config --remove-section --global url.ssh://traindev@gerrit.traindev:29418/ 2>/dev/null || true

# Using this complicated approach instead of git review since git
# review doesn't seem to have an anonymous mode of operation.  If that
# turns out not to be true, please simplify this!

if [ "$patchset" ]; then
    # Use a specific patchset
    eval "$(curl -s "https://gerrit.wikimedia.org/r/changes/$change?o=ALL_REVISIONS&o=DOWNLOAD_COMMANDS" | tail -n+2 | jq -r ".revisions[] | if ._number==$patchset then .fetch[\"anonymous http\"].commands[\"Cherry Pick\"] else \"\" end")"
else
    # Use latest patchset
    eval "$(curl -s "https://gerrit.wikimedia.org/r/changes/$change?o=CURRENT_REVISION&o=DOWNLOAD_COMMANDS" | tail -n+2  | jq -r '.revisions[.current_revision].fetch["anonymous http"].commands["Cherry Pick"]')"
fi
