PATH="/workspace/venvs/scap/bin:/workspace/release/bin:${PATH}"
# Ensure that when we're using Python's "requests" module from
# /workspace/venvs/scap, we use the system CA bundle instead
# of the one that comes bundled in "certifi" module in the venv.
export REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt
