<?php

$wgDBserver = 'db';
$wgDBuser = 'wikiuser';
$wgDBpassword = 'password';

$wmgPasswordSecretKey = 'traindev-password-secret-key-not-so-secret';

$wmgCaptchaSecret = '*';
$wmgCaptchaPassword = '*';

$wmgLogstashPassword = '*';

$wmgRedisPassword = '*';

$wmgTranslationNotificationUserPassword = '*';

$wmgVERPsecret = '*';

$wmgElectronSecret = null;

$wmgContentTranslationCXServerAuthKey = null;
$wmgSessionStoreHMACKey=null;

$wmgSwiftConfig = [];
$wmgSwiftConfig['dev'] = [
        'cirrusAuthUrl' => null,
        'cirrusUser' => null,
        'cirrusKey' => null,
        'thumborUser' => null,
        'thumborPrivateUser' => null,
        'thumborUrl' => null,
        'thumborSecret' => null,
        'user' => null,
        'key' => null,
        'tempUrlKey' => null,
];
