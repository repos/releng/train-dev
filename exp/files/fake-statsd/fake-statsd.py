import socket
import logging


def main():
    logging.basicConfig(level=logging.INFO, format="%(asctime)s %(message)s")

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = ("0.0.0.0", 8125)
    sock.bind(server_address)
    logging.info(
        f"Fake statsd listener started on {server_address[0]}:{server_address[1]}"
    )

    try:
        while True:
            data, peeraddress = sock.recvfrom(4096)
            message = data.decode("utf-8")
            logging.info(message)
    except KeyboardInterrupt:
        logging.info("Fake statsd listener shutting down")
    finally:
        sock.close()


if __name__ == "__main__":
    main()
