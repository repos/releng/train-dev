# -*- coding: utf-8 -*-

import argparse
import logging
import time
import requests
from requests.utils import requote_uri, get_netrc_auth
import json


def load(res):
    try:
        res.raise_for_status()
    except requests.exceptions.RequestException as e:
        logging.warning(
            "Request Failed: %s %s %s" % (res.url, res.status_code, res.text)
        )
        raise e

    try:
        # gerrit prepends junk to the response, strip it:
        json_str = res.text[len(")]}'\n") :]
        data = json.loads(json_str)
        return data
    except ValueError as e:
        logging.warning("Could not decode response: %s" % res.text)
        raise e


def query_gerrit(query: str) -> dict:
    url = requote_uri(f"http://gerrit.traindev:8080/changes/?q={query}")
    return load(requests.get(url))


def get_open_changes():
    return query_gerrit("status:open -is:wip label:Verified=0")


def get_submittable_changes():
    return query_gerrit("label:Verified=2 is:submittable is:mergeable -is:wip")


def verify(change_id):
    session = requests.Session()
    session.auth = get_netrc_auth("http://gerrit.traindev:8080/a")
    url = (
        "http://gerrit.traindev:8080/a/changes/%s/revisions/current/review" % change_id
    )
    payload = {"labels": {"Verified": +2}}
    load(session.post(url, json=payload))


def submit(change_id):
    session = requests.Session()
    session.auth = get_netrc_auth("http://gerrit.traindev:8080/a")
    url = "http://gerrit.traindev:8080/a/changes/%s/submit" % change_id
    return load(session.post(url))


def process_changes(operation, query_func, operation_func):
    change_numbers = [change["_number"] for change in query_func()]
    logging.info(f"Changes to {operation}: {change_numbers}")
    for change_number in sorted(change_numbers):
        logging.info(f"{operation} change {change_number}")
        try:
            operation_func(change_number)
        except requests.exceptions.RequestException:
            pass


def process_open_changes():
    process_changes("verify", get_open_changes, verify)


def process_submittable_changes():
    process_changes("submit", get_submittable_changes, submit)


def main():
    logging.basicConfig(level=logging.INFO, format="%(asctime)s %(message)s")

    parser = argparse.ArgumentParser()
    parser.add_argument("--interval", help="The polling interval", default=30, type=int)
    args = parser.parse_args()

    logging.info(
        f"Verifier/submitter started.  Polling interval is {args.interval} seconds"
    )
    while True:
        try:
            process_open_changes()
        except Exception as e:
            logging.warning("While processing open changes:")
            logging.warning(e)

        try:
            process_submittable_changes()
        except Exception as e:
            logging.warning("While processing submittable changes:")
            logging.warning(e)

        time.sleep(args.interval)


if __name__ == "__main__":
    main()
