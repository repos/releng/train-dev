#!/bin/bash

set -eu -o pipefail

/entrypoint.setup-run

mkdir -p /run/sshd
/usr/sbin/sshd -D -e
