# Make sure we're running a fresh copy of the train-dev environment.
def prepare_train_dev(ctx):
    # We delete /srv/mediawiki-staging/php-* to get rid of any
    # previously run trains. This accidentally tests that we can log
    # into deploy via the outer VM, but that's incidental, and not the
    # actual test for that.
    ssh_exec(
        ctx, target="deploy", command="cd /srv/mediawiki-staging && sudo rm -rf php-*"
    )


# Run command on a host, accessing it directly.
def ssh_exec(ctx, target=None, command=None):
    _ssh(ctx, target, ["sh", "-euxc", command])


# Run ssh with options and arguments on a target. Add the mutated
# config from source tree.
def _ssh(ctx, target, args):
    runcmd = globals()["runcmd"]
    srcdir = globals()["srcdir"]
    exit_code_zero = globals()["exit_code_zero"]
    cmdlist = ["%s/train-dev" % srcdir, "ssh", target] + args
    runcmd(ctx, cmdlist)
    exit_code_zero(ctx)


# Remember which ssh target to log into for later remote command.
def ssh_login(ctx, target=None):
    ctx["ssh-target"] = target


# Remember which directory to cd into before each command.
def set_remote_dir(ctx, directory=None):
    ctx["remote-dir"] = directory


def collect_command_to_run(ctx, command=None):
    if ctx.get("commands") is None:
        ctx["commands"] = []

    ctx["commands"].append(command)


def run_collected_commands_successfully(ctx):
    exit_code_zero = globals()["exit_code_zero"]
    # print("Will ssh to %s, cd to %s, and run %s" % (ctx["ssh-target"], ctx["remote-dir"], ctx["commands"]))
    ssh_exec(
        ctx,
        target="deploy",
        command="cd %s && %s" % (ctx["remote-dir"], " && ".join(ctx["commands"])),
    )
    exit_code_zero(ctx)


# Verify pathname is a directory on remote server.
def ssh_isdir(ctx, pathname=None):
    pass


# Verify pathname is a regular file on remote server.
def ssh_isfile(ctx, pathname=None):
    pass


# Verify pathname is a symlink on remote server.
def ssh_islink(ctx, pathname=None):
    pass


# Verify pathname and anything under it are owned by a user.
def ssh_recursive_owner(ctx, pathname=None, username=None):
    pass


# Verify pathname and anything under it are owned by a group.
def ssh_recursive_group(ctx, pathname=None, group=None):
    pass


# Check that stdout of latest runcmd is empty.
def stdout_is_empty(ctx, pattern=None):
    assert_eq = globals()["assert_eq"]
    stdout = ctx.get("stdout", "")
    if stdout:
        print("stdout:", repr(stdout))
        print("ctx:", ctx.as_dict())
    assert_eq(stdout, "")
