#!/usr/bin/env python3

import collections
import datetime
import json
import logging
import os
import shlex
import subprocess
import sys


class TrimImages:
    def __init__(self, target: str, logger):
        assert target in ["minikube", "deploy"]
        self.target = target
        self.logger = logger

    def run(self):
        self.logger.info("Processing %s", self.target)
        self.logger.info("docker image prune")
        subprocess.check_call(self._make_remote_cmd(["docker", "image", "prune", "-f"]))

        images = collections.defaultdict(list)

        self.logger.info("Collecting image information")

        # Group images by name
        for line in subprocess.check_output(
            self._make_remote_cmd(
                ["docker", "image", "ls", "--format", "{{ json . }}"]
            ),
            text=True,
        ).splitlines():
            image = json.loads(line)
            repo = image["Repository"]
            image["CreatedAt"] = datetime.datetime.strptime(
                image["CreatedAt"], "%Y-%m-%d %H:%M:%S +0000 UTC"
            )
            images[repo].append(image)

        for repo, images in images.items():
            if repo == "<none>":
                continue

            # Select all but the last image for removal.
            for image in sorted(images, key=lambda image: image["CreatedAt"])[:-1]:
                self._remove_image(image)

    def _make_remote_cmd(self, cmd: list) -> list:
        if self.target == "minikube":
            return ["minikube", "--profile=traindev", "ssh"] + [
                " ".join(map(shlex.quote, cmd))
            ]
        if self.target == "deploy":
            return [
                os.path.join(os.path.dirname(sys.argv[0]), "train-dev"),
                "ssh",
                "deploy",
            ] + cmd
        raise Exception("this should never happen")

    def _remove_image(self, image) -> bool:
        repo = image["Repository"]
        tag = image["Tag"]

        fqin = repo
        if tag != "<none>":
            fqin = f"{fqin}:{tag}"

        p = subprocess.run(
            self._make_remote_cmd(["docker", "image", "rm", fqin]),
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            text=True,
        )
        if p.returncode == 0:
            self.logger.info("Removed %s:\n%s", fqin, p.stdout)
            return True

        if "Error response from daemon: No such image: " in p.stdout:
            return False

        if (
            "Error response from daemon: conflict: unable to remove repository reference"
            in p.stdout
        ):
            self.logger.debug("Unable to remove %s (in-use). Skipping", fqin)
            return False

        raise Exception(f"docker image rm {fqin} failed:\n{p.stdout}")


def main():
    """
    Removes all but the most recent image (by creation date) for each image name
    found in minikube and the deploy container.
    """
    logging.basicConfig(
        format="%(asctime)s %(message)s",
        level=logging.INFO,
        datefmt="%Y/%m/%d %H:%M:%S",
    )
    logger = logging.getLogger(__name__)

    for target in ["minikube", "deploy"]:
        TrimImages(target, logger).run()


if __name__ == "__main__":
    main()
