We would love for you to contribute to the train-dev project, in any
way or form that is good for you. Here are some suggestions to make it
as easy as possible for us to incorporate your contribution. However,
you contributing is more important than you following the guidelines
below.

**Reporting issues, or raising topics for discussion:** Please use the
Phabricator issue tracker, using the "MediaWiki Train Developmnt
Environment" tag.

If there's an existing issue that's clearly for what you want to talk
about, please use that, but if not, it's preferable to open a new
issue, as it's easier for the project maintainers to merge issues than
split them.

**Asking for help:** Please use the Phabricator issue tracker for this
as well.

**Updating documentation, tests, or code:** Please submit a change via
Gerrit against the mediawiki/tools/train-dev repository.

**Portability:** At this time we care about train-dev working on
Debian 10 (Buster) and Mac OS with Docker Desktop.

**Python version:** We require Python code to work on Python 3.4 and
later, the newest version we can rely on in WMF production at this
time. Please don't add Python 2 code.

**Keep commits small:** It's easier for us to review smaller commits,
less than about 200 lines of diff, except when each hunk is similar.
For example, if you rename a function, a commit that does that and
nothing else can be quite large, but is easy to review.

**Keep commits integral:** Each commit should be a cohesive change,
not a mix of unrelated changes. If a commit renames a function, it
shouldn't also add a new argument to the function or make other
changes unrelated to the rename.

**Keep commits in a merge request related:** If a commit adds a new
feature, it shouldn't also fix a bug in an old feature. If there's a
need to re-do the new feature, the bug fix will take longer to get
merged.

**Tell a story with a merge request:** The commits in a merge request
should be arranged in a way that make the change as a whole easy to
follow: they "tell a story" of how you would make the set of changes
the second time, knowing everything you know after doing them the
first time.

The goal is not to show the path you originally took, but show how to
get there in the best possible way. You should tell the story of
flying by plane to get somewhere, not how you explored the world and
eventually invented flying machines to travel faster.
