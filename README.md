# Wikimedia weekly train deployment: safe place to test and play

[train]: https://wikitech.wikimedia.org/wiki/Heterogeneous_deployment/Train_deploys

This is an environment for developing WMF weekly
deployment ("[train][]") tooling and processes.

## Outline

* This system uses a composition of Docker containers to represent
  various machines that are used in production:

  - deployment server
  - gerrit server, with the relevant source code included
  - db server, running MariaDB and etcd
  - www server, running Apache and php-fpm
  - kubernetes cluster

## Pre-requisites

### Hardware

You need a host that has enough CPU, RAM, disk, and network
bandwidth to run these things. Recommendations:

- 8 hyperthreads
- 16 gigs of RAM
- 25 of gigabytes of available disk space
- fast external network

Less may be enough, possibly at some speed penalty.

### Supported operating systems

- Debian Linux 10/11
- Mac OS (Tested with 12.5.1)

### Dependencies

The host must have Python 3.6+, Docker, the Docker Compose plugin, minikube, and make
installed.

On Debian, follow the intructions at https://docs.docker.com/engine/install/debian/ to install Docker, then:
~~~sh
sudo apt-get install -y docker-compose-plugin make
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube && rm minikube-linux-amd64
~~~

On a Mac, homebrew can be used to install the necessary packages.
To install homebrew, run the following command:
~~~sh
bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
~~~

To install the packages, run the following:
~~~sh
brew install make docker docker-compose minikube
~~~


To run tests you need Subplot installed. See
<https://subplot.liw.fi/download/> for instructions on how to install
the Debian package.

## Instructions

To build and start train-dev containers:

~~~sh
./train-dev build /workspace
~~~

Replace `/workspace` with a directory suitable for storing a large
amount of data (mirror of some Gerrit repos and the /srv filesystems
of containers). This should take less than 10 minutes, depending on
the speed of your computer and network, and whether or not you've
performed a build before.

If you locally have a tree of Gerrit repositories, pass the base directory to
have train-dev clone from the local copy before refreshing from Gerrit.
Assuming one has the repositories cloned under `~/projects` with repositories
such as `~/projects/mediawiki/core`, `mediawiki/skins/Vector` ... Pass
`~/projects` as a third argument:

~~~sh
./train-dev build /workspace ~/projects
~~~

To stop train-dev containers:

~~~sh
./train-dev stop
~~~

To start previously built containers:

~~~sh
./train-dev start /workspace
~~~

You can log into the deploy container:

~~~sh
$ ./train-dev ssh deploy
~~~

You can run the train on the deploy host:

~~~sh
$ ./train-dev ssh deploy
debian@deploy:~$ ./train
~~~

Then you can deploy to kubernetes on the deploy host:
~~~sh
$ ./train-dev ssh deploy
debian@deploy:~$ cd /srv/deployment-charts/helmfile.d/services/mw-debug
debian@deploy:/srv/deployment-charts/helmfile.d/services/mw-debug$ helmfile -e traindev apply
~~~

You can keep your repos and Gerrit up to date:

~~~sh
$ ./train-dev update-repos /workspace
~~~

You can go to http://localhost:8080 to access the Gerrit UI.

To build train-dev and run its acceptance tests:

~~~sh
./check /workspace yes
~~~

## Hacking on scap and other tools

The `deploy` and `www` containers are configured to bind mount `/workspace`
where both the `repos/releng/scap` and `repos/releng/release` projects
have been cloned. The containers are also configured with `PATH` and `SCAP`
environment variables pointing to these copies, so all you need to do to
hack/test is edit files under `/workspace/{scap,release}` and run the commands
on `deploy` as you normally would on the deployment server.

Have fun. Send patches. Report problems.
