# Introduction

The "train-dev" environment is meant to provide a place where the
tools and processes of deploying MediaWiki and configuration to
Wikimedia production can be safely practised, tested, and experimented
with, without endangering production sites.

The Wikimedia Foundation deploys MediaWiki and related software to
production using a weekly deployment cycle called "the train". The
process steps are described at [Train deploys][] and summarised here.

[Train deploys]: https://wikitech.wikimedia.org/wiki/Heterogeneous_deployment/Train_deploys

On Tuesday the week's branch is created based on current state of the
main line of development in each included git repository (MediaWiki
core and each extension), and this is deployed to a small number of
wikis that get little traffic, as a smoke test.

On Wednesday, it gets deployed to a larger group of small wikis that
get more real traffic. On Thursday, to all wikis, including the
biggest one, which is English-speaking Wikipedia.

If anything goes wrong, a "roll back" is performed to use the previous
week's version instead, for one or more groups of wikis.

This is all a fairly manual process. It is also very stressful to the
"train conductor". Part of why it is so stressful is that no way to
train people except by doing the deployments in production.

Release Engineering also maintains the tooling to do these
deployments. There is no production-like environment in which to test
the tools or changes to them.

Release Engineering is also to a great part responsible for how the
deployments happens, but has no place where to do experiments.

The train-dev environment will fix all of this and also there will be
puppies, kittens, and rainbow unicorns. And pudding.

This document explains what the acceptance criteria for the train-dev
environment are, and how they're verified. It also explains how
train-dev is implemented.

## Current status

The first manual step in the train, "scap prep [VERSION]", works, for
a specific weekly branch that's mirrored into the train-dev
environment. The rest is very much work in progress.



# High level requirements

This chapter explains the high-level requirements for train-dev. These
are requirements which guide the whole project, but for which it
doesn't make sense to write automatically verifiable acceptance
criteria for.

* Train-dev should be usable by each user in isolation from others, so
  that any changes they may make don't bother other people.

* Train-dev should be possible to run as a shared environment, so that
  it can be better used for training and collaboration.

* Train-dev should be sufficiently similar to actual WMF production
  environments that the actual deployment tooling used can be used,
  and the tools don't notice the difference. If something works in
  train-dev, it should likely work in production, and vice versa.

* Train-dev should be entirely separate from WMF production systems.
  It must not be possible to affect WMF production systems by using
  train-dev.


# Architecture and implementation

Train-dev is implemented using nested Linux virtual machines: virtual
machines running inside a virtual machine. The inner VMs simulate the
various servers and services in WMF production environments that are
relevant to the weekly deployments, if at a lesser scale. The outer VM
encapsulates the inner VMs for easier deployment: the train-dev user
only needs to get one VM running, the outer one. That VM takes care of
the rest.

There are four inner VMs:

* **git** is the git server, which stands in for the WMF Gerrit
  server, **gerrit.wikimedia.org**. It is seeded with a copy of the
  relevant git repositories needed to run the train. However, the git
  repositories are not automatically refreshed from Gerrit, as the
  main focus of train-dev is on the tooling and processes, and one
  week's MediaWiki seems as good as the next's.

* **deploy** stands in for the **deploy1001.eqiad.wmnet** server,
  where all the deployment commands are run.

* **db** runs MariaDB (for MediaWiki) and etcd (for MW config).

* **www** runs Apache2, NGINX (for SSL termination), and php-fpm.
  It stands in for the various **mwXXXX** servers.

The outer VM and inner VMs run Debian 10 (buster), except deploy and
www which run Debian 9 (stretch) to be compatible with the
corresponding real servers. The VMs are built based on the Debian
cloud images for OpenStack that Debian builds and publishes, to avoid
having us build our own images.

The outer VM runs the libvirt virtualisation system, using the Linux
kvm kernel module for efficient virtualisation. libvirt was chosen
because of features and familiarity.

The various VMs are provisioned (configured) using Ansible. This is
different from what WMF uses for the real servers, but Release
Engineering found it too difficult to use the WMF Puppet automation
for train-dev. This may have to change later on.

The train-dev git repository has scripts to automatically build a
train-dev VM. They require an environment like Debian 10 (buster),
with qemu and related tools (such as qemu-img) installed.



# Acceptance criteria

This chapter documents the detailed acceptance criteria for train-dev
and how to automatically verify they are met by the train-dev
implementation. This is expressed as given/when/then scenarios,
similar to the Cucumber tool, but in a form meant for the [Subplot][]
tool.

[Subplot]: https://subplot.liw.fi/

Subplot generates a test program, which actually executes the
scenarios and verifies them. The test program is pointed at a running
instance of train-dev.

## User can log into git VM

~~~scenario
given a running train-dev
when I log into git via train-dev to run /bin/true
then program finished successfully
~~~


## User can log into deploy VM

~~~scenario
given a running train-dev
when I log into deploy via train-dev to run /bin/true
then program finished successfully
~~~


## User can run train

~~~scenario
given I am logged into the deploy host
and I am in the /home/debian directory
when I run eval $(ssh-agent)
and I run ~/blast
and I run ~/train
then it all finishes successfully
~~~


# Acceptance for the deploy server

This chapter documents the detailed acceptance criteria for the
deployment server (deploy) in train-dev. Deploy is meant to closely
match the deployment server in production (deploy1001.eqiad.wmnet).
These scenarios capture what that means in detail.


## Deploy runs Debian 9 (stretch)

Currently the deployment server runs Debian 9. When the production one
gets upgraded, train-dev needs to change.

~~~scenario
given a running train-dev
when I log into deploy via train-dev to run lsb_release -cs
then program finished successfully
and output matches "stretch"
~~~


## Deploy has /srv/mediawiki-staging

~~~scenario
given a running train-dev
when I log into deploy
then I see /srv/mediawiki-staging is a directory
~~~

## Deploy user is in the right groups

~~~scenario
given a running train-dev
when I log into deploy via train-dev to run groups
then output matches "wikidev"
and output matches "deployment"
~~~

## Deploy user git is configured correctly

As part of the train the deployer needs to make git commits, and for
that git needs to have user configuration.


~~~scenario
given a running train-dev
when I log into deploy via train-dev to run git config user.name
then output matches "Train Conductor"
when I log into deploy via train-dev to run git config user.email
then output matches "tconductor@wikimedia.org"
~~~

We further check that we can actually make a git commit and push successfully.

~~~scenario
given I am logged into the deploy host
and I am in the /tmp directory
when I run rm -fr /tmp/release
and I run git clone https://gitlab.wikimedia.org/repos/releng/release.git
and I run cd /tmp/release
and I run echo This is a test > testfile
and I run git add testfile
and I run git commit -m "Test commit"
and I run git push origin HEAD
and I run git reset --hard HEAD~1
and I run git push origin +HEAD
and I run cd
and I run rm -fr /tmp/release
then it all finishes successfully
~~~



---
title: A train environment for WMF
author: Release Engineering
template: python
bindings:
  - subplot/train-dev.yaml
  - subplot/runcmd.yaml
functions:
  - subplot/train-dev.py
  - subplot/runcmd.py
...
