BEGIN {
    t=0
    start=0
}

/^TIME:/ && start == 0 {
    start=$2
}

/^TIME:/ {
    print $2-start, $2-t, $3
    t=$2
}
