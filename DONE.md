# Definition of Done for train-dev

In agile software development and other iterative development
methodologies, a "definition of done" (see [DoD][]) is the minimum
list of criteria that an incremental, self-contained change to a
software project needs to fulfill. The DoD answers the question "does
this change need any further work, later?". It is not a definition of
when the project as a whole is finished.

For the train-dev project we have the following definition of done:

* the automated tests for the project pass successfully:
  `./check /path/to/images yes 5555`
* any new or changed functionality is covered by the automated test
  suite, which has been updated to reflect the changes
* any relevant in-repository documentation (README.md, train-dev.md,
  or similar) has been updated to reflect the changes
* all changes are merged into the master branch
* any relevant Phabricator tickets have been updated, especially their
  status

This definition will be updated via consensus by train-dev developers,
as needed.

[DoD]: https://www.agilealliance.org/glossary/definition-of-done/
