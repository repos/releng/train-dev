#!/usr/bin/env python3

import argparse
import os
import queue
import re
import subprocess
import sys
import threading
import time

# This code targets python 3.6+
if sys.version_info < (3, 6):
    raise SystemExit(__file__ + " requires python 3.6")

mirror_refs = ["+refs/heads/*:refs/heads/*", "+refs/tags/*:refs/tags/*"]

gerrit_base = "https://gerrit.wikimedia.org/r/"

allowed_bases = [
    gerrit_base,
    "https://phabricator.wikimedia.org/source/",
    "https://phabricator.wikimedia.org/diffusion/",
    "https://gitlab.wikimedia.org/",
]

gerrit_repos = [
    "mediawiki/core",
    "mediawiki/extensions",
    "mediawiki/skins",
    "operations/mediawiki-config",
    "operations/deployment-charts",
]


def git(args, **kwargs):
    kwargs["check"] = kwargs.get("check", True)
    kwargs["universal_newlines"] = True

    capture_output = False
    if "capture_output" in kwargs:
        capture_output = kwargs["capture_output"]
        del kwargs["capture_output"]

    if capture_output:
        # Note: This is different than stock capture_output behavior
        # (which sets both stdout and stderr to subprocess.PIPE). The way we do it
        # here makes the resulting output easier to read.
        kwargs["stdout"] = subprocess.PIPE
        kwargs["stderr"] = subprocess.STDOUT

    try:
        return subprocess.run(["git"] + args, **kwargs)
    except subprocess.CalledProcessError as e:
        if capture_output:
            print("{} failed:".format(" ".join(e.cmd)))
            print(e.stdout)
        raise


def get_remotes(dir):
    return git(["-C", dir, "remote", "show"], capture_output=True).stdout.splitlines()


def remote_exists(remote, dir):
    return remote in get_remotes(dir)


def get_remote_url(remote, dir):
    return git(
        ["-C", dir, "remote", "get-url", remote], capture_output=True
    ).stdout.strip()


def set_remote_url(remote, repo_url, dir):
    git(["-C", dir, "remote", "set-url", remote, repo_url])


def get_default_branch(repo_url):
    output = git(
        ["ls-remote", "--symref", repo_url, "HEAD"], capture_output=True
    ).stdout
    m = re.search(r"^(ref: refs/heads/.*)\t", output)
    if m:
        return m.group(1)
    else:
        return "ref: refs/heads/master"


def get_submodule_urls(repo_dir, branch="master") -> list:
    """
    'branch' may be a string or a list of strings naming branches that
    should be checked for a .gitmodules file.
    """
    if isinstance(branch, list):
        branches = branch
    else:
        branches = [branch]

    res = set()

    for branch in branches:
        submodules = git(
            ["-C", repo_dir, "cat-file", "-p", f"{branch}:.gitmodules"],
            capture_output=True,
            check=False,
        ).stdout.splitlines()

        for line in submodules:
            m = re.search(r"^\s*url = (.*)", line)
            if m:
                res.add(m.group(1))

    return list(res)


def should_mirror(url):
    for base in allowed_bases:
        if url.startswith(base):
            return True
    return False


def get_repo_name(url):
    for base in allowed_bases:
        if url.startswith(base):
            return url[len(base) :]
    raise Exception("url {} doesn't begin with one of {}".format(url, allowed_bases))


def url_to_repo_dir(url, mirror_dir):
    repo_name = get_repo_name(url)
    path = os.path.join(mirror_dir, repo_name)
    if not path.endswith(".git"):
        path += ".git"

    return path


# Note, not thread safe.  Only for use in the main thread.
def atomic_file_update(filename, text):
    tmpfile = f"{filename}.tmp.{os.getpid()}"
    with open(tmpfile, "w") as f:
        f.write(text)
    os.rename(tmpfile, filename)


def init_repo(repo_url, repo_dir, git_cache, verbose):
    """repo_dir must always end with .git"""

    # Notes:
    # * git init will create directories as needed.
    # * git init can be re-run on an existing repo
    git(["init", "-q", "--bare", repo_dir])

    if remote_exists("origin", repo_dir):
        if get_remote_url("origin", repo_dir) != repo_url:
            set_remote_url("origin", repo_url, repo_dir)
    else:
        if git_cache:
            cache_repo = url_to_repo_dir(repo_url, git_cache)
            if os.path.exists(cache_repo):
                if verbose:
                    print(f"Bootstrapping {repo_dir} from {cache_repo}")
                git(["-C", repo_dir, "fetch", "-q", cache_repo] + mirror_refs)

        git(["-C", repo_dir, "remote", "add", "origin", repo_url])

    git(["-C", repo_dir, "config", "--local", "--unset-all", "remote.origin.fetch"])
    for refspec in mirror_refs:
        git(
            [
                "-C",
                repo_dir,
                "config",
                "--local",
                "--add",
                "remote.origin.fetch",
                refspec,
            ]
        )
    git(["-C", repo_dir, "config", "--local", "remote.origin.mirror", "true"])

    # Set the name of the default branch
    atomic_file_update(
        os.path.join(repo_dir, "HEAD"), get_default_branch(repo_url) + "\n"
    )


def update_repo(repo_dir):
    git(
        ["-C", repo_dir, "remote", "--verbose", "update", "--prune"],
        capture_output=True,
    )


def clone_repo(repo_url, mirror_dir, git_cache, verbose):
    """
    Returns the path to the repo directory, or None if repo_url is not
    a URL that we mirror
    """

    if not should_mirror(repo_url):
        if verbose:
            print(f"Skipping {repo_url} (not in allowed_bases)")
        return None

    repo_dir = url_to_repo_dir(repo_url, mirror_dir)
    # Skipping init_repo() for existing repos saves a lot of time.
    if not os.path.exists(repo_dir):
        init_repo(repo_url, repo_dir, git_cache, verbose)
    if verbose:
        print(f"Updating {repo_dir} from {repo_url}")
    update_repo(repo_dir)
    return repo_dir


def get_latest_train_branches(repo_dir) -> list:
    """
    Return a list of up to two of the latest train branches.
    """
    # Copied from scap utils.py
    BRANCH_RE = re.compile(
        r"(?P<major>\d{1})."
        r"(?P<minor>\d{1,2})."
        r"(?P<patch>\d{1,2})"
        r"-wmf.(?P<prerelease>\d{1,2})"
    )

    r = filter(
        lambda branch: re.search(BRANCH_RE, branch),
        [
            line.split("refs/heads/")[1]
            for line in git(
                ["ls-remote", "--sort=-version:refname", repo_dir, "wmf/*"],
                capture_output=True,
            ).stdout.splitlines()
        ],
    )

    return list(r)[:2]


def clone_worker(q, failures_event, mirror_dir, git_cache, verbose, submodule_strategy):
    while True:
        request = q.get()
        if request == "stop":
            break

        try:
            repo_url = request["url"]
            repo_dir = clone_repo(repo_url, mirror_dir, git_cache, verbose)

            if repo_dir is None:
                continue

            recurse_submodules = request.get("recurse_submodules", False)
            submodules_branch = request.get("submodules_branch", "master")

            repo = get_repo_name(repo_url)
            if repo == "operations/mediawiki-config":
                recurse_submodules = True
            if repo == "mediawiki/core" and submodule_strategy == "train":
                recurse_submodules = True
                submodules_branch = get_latest_train_branches(repo_dir)
            if repo == "mediawiki/extensions" and submodule_strategy == "all":
                recurse_submodules = True
            if repo == "mediawiki/skins" and submodule_strategy == "all":
                recurse_submodules = True

            if recurse_submodules:
                for submodule_url in get_submodule_urls(repo_dir, submodules_branch):
                    enqueue_clone(q, submodule_url, True, submodules_branch)
        except Exception as e:
            failures_event.set()
            print("clone worker caught error while cloning {}".format(repo_url))
            print(e)
        finally:
            q.task_done()


def enqueue_clone(q, url, recurse_submodules=False, submodules_branch="master"):
    q.put(
        {
            "url": url,
            "recurse_submodules": recurse_submodules,
            "submodules_branch": submodules_branch,
        }
    )


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(
        "mirror_directory", help="The base directory to contain mirrored repositories"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Print a line for each repository processed",
        action="store_true",
    )
    parser.add_argument(
        "--strategy",
        help="""Specifies how to determine the set of repos to be mirrored

train (the default):
  Mirror only those extensions and skins that are
  submodules of the most recent mediawiki/core wmf/*
  branch.

all:
  Mirror all available extensions and skins.  This
  strategy takes about double the time and storage of
  the "train" strategy but it is useful if you want to
  test mediawiki deployments off the master branch
  (such as what happens in beta cluster)

""",
        choices=["train", "all"],
        default="train",
    )

    parser.add_argument(
        "-j",
        "--workers",
        help="The maximum number of worker threads",
        type=int,
        default=6,
    )
    parser.add_argument(
        "-c",
        "--git-cache",
        help="Local base directory from which to attempt git cloning before fetching remotely",
    )

    args = parser.parse_args()

    mirror_dir = args.mirror_directory

    q = queue.Queue()
    failures_event = threading.Event()

    workers = [
        threading.Thread(
            target=clone_worker,
            args=(
                q,
                failures_event,
                mirror_dir,
                args.git_cache,
                args.verbose,
                args.strategy,
            ),
        )
        for i in range(args.workers)
    ]
    for worker in workers:
        worker.start()
    try:
        for repo in gerrit_repos:
            repo_url = os.path.join(gerrit_base, repo)
            enqueue_clone(q, repo_url)
        q.join()
    finally:
        for worker in workers:
            q.put("stop")
        for worker in workers:
            worker.join()

    # Create a flag to indicate to Gerrit that mirrors were updated
    updated_flag_file = os.path.join(mirror_dir, ".updated")

    with open(updated_flag_file, "w") as f:
        # Make the flag file world-writeable so that
        # exp/files/gerrit/update-repos (which runs in the gerrit
        # container) can clear the updated flag.
        os.chmod(updated_flag_file, 0o666)
        f.write(f"mirror-repos.py finished at {time.ctime()}\n")

    if failures_event.is_set():
        raise SystemExit("Operation not successful")


if __name__ == "__main__":
    main()
